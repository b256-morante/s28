db.rooms.insertOne({
"name": "single",
 "accommodates": 2,
 "price": 1000,
 "description": "A simple room with all the basic necessities",
 "rooms availabe": 10,
 "isAvailable": "false"   
})

db.rooms.insertMany([
{"name": "double",
  "accommodates": 3,
   "price": 2000,
    "description": "A room fit for a small family going on a vacation",
    "rooms available": 5,
    "isAvailable": "false"},
  {"name": "queen",
  "accommodates": 4,
   "price": 4000,
    "description": "A room with a queen size bed perfect for a simple getaway",
    "rooms available": 5,
    "isAvailable": "false"}
    ])
    
 db.rooms.find({"name": "double"})
 
 db.rooms.updateOne(
    { "name": "queen" },
    {$set : {"rooms available": 0}}
)
   

 db.rooms.deleteMany({"rooms available": 0})
 
 